<?php
/**
 * Plugin Name: Xpand Contact Form
 * Bitbucket Plugin URI: https://bitbucket.org/tomxpand/wordpress-plugin-contact-form
 * Description: Adds Contact Form shortcode & funtionality
 * Version: 0.21
 * Author: Xpand Marketing
 * Author URI: http://www.xpandmarketing.co.uk/
 */

include_once('../github-updater-develop/updater.php');

if (is_admin()) {
    $config = array(
        'slug' => plugin_basename(__FILE__),
        'proper_folder_name' => 'xpand-contact-form',
        'api_url' => 'https://bitbucket.org/api/1.0/repositories/tomxpand/wordpress-plugin-contact-form/',
        'raw_url' => 'https://bitbucket.org/tomxpand/wordpress-plugin-contact-form/raw/dfb58c6461393048e7f5f43e4f667c96d78c61c5/',
        'github_url' => 'https://bitbucket.org/tomxpand/wordpress-plugin-contact-form',
        'zip_url' => 'https://bitbucket.org/tomxpand/wordpress-plugin-contact-form/get/dfb58c646139.zip',
        'sslverify' => true,
        'requires' => '3.0',
        'tested' => '3.3',
        'readme' => 'README.md',
    );
    new WP_GitHub_Updater($config);
}

/**
 * function xCF_add_shortcode
 * @since Version 0.2
 */
function xCF_add_shortcode( $atts ) {
	extract( shortcode_atts(
		array(
			'send_to' => 'admin_email',
			'send_from' => 'site_name Contact Form',
			'fields' => 'name,number,email,message',
		), $atts ));

	$availableFields = array(
		'name' => array(
			'name' => 'xcf_name',
			'class' => 'xcf_textinput xcf_name',
			'type' => 'text',
			'placeholder' => 'Name'
		),
		'email' => array(
			'name' => 'xcf_email',
			'class' => 'xcf_emailinput xcf_name',
			'type' => 'email',
			'placeholder' => 'Email'
		),
		'tel' => array(
			'name' => 'xcf_tel',
			'class' => 'xcf_telinput xcf_name',
			'type' => 'tel',
			'placeholder' => 'Number'
		),
		'message' => array(
			'name' => 'xcf_message',
			'class' => 'xcf_textarea xcf_name',
			'type' => 'textarea',
			'placeholder' => 'Message'
		),
	);

	if($_POST['submit'] == "true") {
		//submit form
	} else {
		$return = '<form action="" method="POST" id="xcf_contact">';

		foreach ($availableFields as $field) {
			$placeholder = ($field['placeholder'] ? $placeholder : '');

			if($field['type'] == 'textarea') {
				$return .= '<textarea name="'.$field['name'].'" class="'.$field['class'].'" '.$placeholder.'></textarea>';
			} else {
				$return .= '<input type="'.$field['type'].'" name="'.$field['name'].'" class="'.$field['class'].'" '.$placeholder.'></textarea>';
			}
		}

		$return .= '<button class="xcf_submit" name="submit" value="true">Submit</button></form>';
	}

	return $return;
}
add_shortcode( 'contact-form', 'xCF_add_shortcode' );

/**
 * function xCF_send_mail
 * @since Version 0.2
 */
function xCF_send_mail() {

}